class CardTimeLeftWidget: FrameLayout {
    private lateinit var binding: WidgetCardTimeLeftBinding

    @JvmOverloads
    constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
    )
            : super(context, attrs, defStyleAttr) {
        setupView(context, attrs, defStyleAttr)
    }

    private fun setupView(context: Context, attributeSet: AttributeSet?, defStyleAttr: Int) {
        binding = WidgetCardTimeLeftBinding.inflate(LayoutInflater.from(getContext()), this, true)
    }

    fun setTime(time: String?){
        setTimeCoupon(Date(), time)
//        binding.timeLeft.text = ""
    }

    private fun setTimeCoupon(now: Date, usableExpiredAt: String?, hide: Boolean = true){
        val text = DateUtility.getDateCardMainLeft(Date(), usableExpiredAt, context)
        if (text.isEmpty()){
            if (hide){
                binding.layoutCardTimeLeft.gone()
            }
            else{
                binding.layoutCardTimeLeft.invisible()
            }

        }
        else{
            binding.layoutCardTimeLeft.visible()
            binding.timeLeftCardTimeLeft.text = text
        }
        binding.layoutCardTimeLeft.postDelayed({
            binding.layoutCardTimeLeft.invalidate()
            binding.timeLeftCardTimeLeft.invalidate()
        }, 500)

    }

    fun setTextBySize(size:CardEnumSize){
        when(size){
            CardEnumSize.SMALL ->{
                binding.timeLeftCardTimeLeft.setStyle(R.style.expireDate_card_small)
            }else ->{
                binding.timeLeftCardTimeLeft.setStyle(R.style.expireDate_card_big)
            }
        }
    }
}
