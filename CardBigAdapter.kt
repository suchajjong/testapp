class CardBigAdapter (private var data: MutableList<CardMainModel>): RecyclerView.Adapter<CardBigAdapter.ViewHolder>(){
    private var eventLikeClickListener: ((like: Boolean, model: CardMainModel) -> Unit)? = null
    fun setOnLikeClickListener(listener: (like: Boolean, model: CardMainModel) -> Unit) {
        eventLikeClickListener = listener
    }

    private var eventNotifyClickListener: ((notify: Boolean, model: CardMainModel) -> Unit)? = null
    fun setOnNotifyClickListener(listener: (notify: Boolean, model: CardMainModel) -> Unit) {
        eventNotifyClickListener = listener
    }

    private var eventItemClickListener: ((model: CardMainModel) -> Unit)? = null
    fun setOnItemClickListener(listener: (model: CardMainModel) -> Unit) {
        eventItemClickListener = listener
    }

    fun refreshList(listItem: List<CardMainModel>){
        this.data = listItem.toMutableList()
        notifyDataSetChanged()
    }
    fun refreshSomeItem(item: CardMainReduxModel){
        for (i in (0 until data.size)){
            if (item.id == data[i].id){
                data[i].updateByRedux(item)
                notifyItemChanged(i)
            }
        }
    }

    fun addList(listItem: List<CardMainModel>?){
        val mockList = arrayListOf<CardMainModel>()
        if (listItem == null){
            return
        }
        for (i in listItem){
            if (i.id in data.map { it.id }){}
            else if(i.id in mockList.map { it.id })
            else{ mockList.add(i) }
        }
        val itemCount = this.data.size
        this.data.addAll(mockList)
        notifyItemRangeInserted(itemCount, this.data.size)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = AdapterCardBigBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(data[position])
        Log.d("tag3", "onBindViewHolder: ${position}")
    }

    override fun getItemCount(): Int {
        return data.size
    }

    inner class ViewHolder(private val binding: AdapterCardBigBinding): RecyclerView.ViewHolder(binding.root){
        val context = binding.root.context
        fun bind(model: CardMainModel){
            binding.cv.setModel(model, CardEnumSize.BIG)
            binding.cv.setOnLikeClickListener {
                eventLikeClickListener?.invoke(it, model)
            }
            binding.cv.setOnNotifyClickListener {
                eventNotifyClickListener?.invoke(it, model)
            }
            binding.layout.setOnClickListener {
                eventItemClickListener?.invoke(model)
            }
        }
    }

}
