class CardTextEventWidget: FrameLayout {
    private lateinit var binding: WidgetCardTextEventBinding

    @JvmOverloads
    constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
    )
            : super(context, attrs, defStyleAttr) {
        setupView(context, attrs, defStyleAttr)
    }

    private fun setupView(context: Context, attributeSet: AttributeSet?, defStyleAttr: Int) {
        binding = WidgetCardTextEventBinding.inflate(LayoutInflater.from(getContext()), this, true)
    }

    fun setModel(size: CardEnumSize,model : CardMainModel){
        binding.title.text = model.title
        binding.namePlatform.text = model.brand?.name

        GlideApp.with(context)
            .load(model.brand?.imageUrl)
            .centerCrop()
            .placeholder(GlidePlaceHolder.getPlaceHolder())
            .error(GlidePlaceHolder.getError())
            .skipMemoryCache(false)
            .into(binding.imgPlatform)
        //setPrice(model,size)
        binding.pickUpPrice.text = model.promotionText
        binding.cardTimeLeft.setTime(model.endDate)
        val isShowFreeShip = model.isFreeShipping?:false
        if(!isShowFreeShip){
            binding.FreeShipping.gone()
        }else{
            binding.FreeShipping.visible()
        }
        when(size){
            CardEnumSize.SMALL -> {
                setTextStyleBySmallSize()
                setPrice(model,size)
            }
            else -> {
                setTextStyleByBigSize()
                setPrice(model,size)
            }
        }
    }

    private fun setPrice(model : CardMainModel,size : CardEnumSize){
        if(model?.price == null && model?.fullPrice == null){
            setShowPartOfPrice(size)
        }else{
            binding.partOfPriceNumber.visible()
            binding.price.text = "฿${model.price.toString().toNumberComma2DigitAuto()}"
            binding.fullPrice.addStrike()
            binding.fullPrice.text = "฿${model.fullPrice.toString().toNumberComma2DigitAuto()}"
            model.price?.let { binding.price.visible() }?:kotlin.run{binding.price.invisible()}
            model.fullPrice?.let { binding.fullPrice.visible() }?:kotlin.run{binding.fullPrice.invisible()}
        }

    }

    private fun setShowPartOfPrice(size : CardEnumSize){
        when(size){
            CardEnumSize.SMALL ->{
                binding.partOfPriceNumber.invisible()
            }else ->{
                binding.partOfPriceNumber.gone()
            }
        }
    }

    private fun setTextStyleByBigSize(){
        binding.apply {
            this.title.setStyle(R.style.title_card_big)
            this.price.setStyle(R.style.price_card_big)
            this.fullPrice.setStyle(R.style.fullPrice_card_big)
            this.namePlatform.setStyle(R.style.brand_card_big)
            this.cardTimeLeft.setTextBySize(CardEnumSize.BIG)
        }
    }

    private fun setTextStyleBySmallSize(){
        binding.apply {
            this.title.setStyle(R.style.title_card_small)
            this.price.setStyle(R.style.price_card_small)
            this.fullPrice.setStyle(R.style.fullPrice_card_small)
            this.namePlatform.setStyle(R.style.brand_card_small)
            this.cardTimeLeft.setTextBySize(CardEnumSize.SMALL)
        }
    }


}
