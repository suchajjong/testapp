class CardMainWidget: FrameLayout {
    private lateinit var binding: WidgetCardMainBinding

    private var dimensionRatio = "H,3:2"

    private var model: CardMainModel? = null
    private var size: CardEnumSize? = null

    private var eventLikeClickListener: ((like: Boolean) -> Unit)? = null
    fun setOnLikeClickListener(listener: (like: Boolean) -> Unit) {
        eventLikeClickListener = listener
    }

    private var eventNotifyClickListener: ((notify: Boolean) -> Unit)? = null
    fun setOnNotifyClickListener(listener: (notify: Boolean) -> Unit) {
        eventNotifyClickListener = listener
    }

    @JvmOverloads
    constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
    )
            : super(context, attrs, defStyleAttr) {
        setupView(context, attrs, defStyleAttr)
    }

    private fun setupView(context: Context, attrs: AttributeSet?, defStyleAttr: Int) {
        binding = WidgetCardMainBinding.inflate(LayoutInflater.from(getContext()), this, true)
        setListener()
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.CardMainWidget)
        dimensionRatio = typedArray.getString(R.styleable.CardMainWidget_custom_constraintDimensionRatio)?:"H,3:2"
        setDimensionRatio()
        typedArray.recycle()
    }
    private fun setListener(){
        binding.cardLike.setOnClickListener {
            eventLikeClickListener?.invoke(it)
        }
    }
    fun setModel(model: CardMainModel, size: CardEnumSize){
        this.model = model
        this.size = size
        if (model.getEnumType() == CardEnumType.OTHER){
            binding.typeOther.visible()
            return
        }
        else{
            binding.typeOther.gone()
        }
        binding.discount.setDisCount(model.discountPercent)
        binding.cardTag.setModel(model.type?:"shopper", model.promotionType?:"")
        binding.cardLike.setLike(model.isLove?:false)
        binding.cardLike.setLikeCount(model.countLove?:0)
        binding.rank.setRank(null)
        setImg(model.coverUrl?:"", size, model)
        if (model.isDoubledigit==true){
            binding.tagFestival.visible()
        }
        else{
            binding.tagFestival.gone()
        }
        binding.cardLike.visible()
        when(model.getEnumType()){
            CardEnumType.EVENT -> {setTypeEvent()}
            CardEnumType.PROMOTION -> {setTypePromotion()}
            CardEnumType.ARTICLE -> {setTypeArticle()}
            CardEnumType.AFFILIATE -> {
                setTypeAffiliate()
                //mock rank
                //binding.rank.setRank(3)
            }
            CardEnumType.AFFILIATE_FLASH_SALE_NOW -> {setTypeAffiliateFlashSale()}
            CardEnumType.AFFILIATE_FLASH_SALE_SPECIAL -> {setTypeAffiliateFlashSaleSpecial()}
            CardEnumType.AFFILIATE_FLASH_SALE_BEFORE -> {setTypeAffiliateFlashSaleSoon()}
            CardEnumType.MALL -> {setTypeMall()}
        }

    }
    private fun setImg(url: String, size: CardEnumSize = CardEnumSize.BIG, model: CardMainModel){
        when(size){
            CardEnumSize.SMALL -> {binding.layoutImg.setImgFix(url)}
            else -> {binding.layoutImg.setImgAuto(url, model)}
        }
    }

    private fun hideAllCardText(){
        binding.cardTextEvent.gone()
        binding.cardTextAffiliate.gone()
        binding.cardTextAffiliateFlashSale.gone()
        binding.cardTextAffiliateFlashSaleSoon.gone()
        binding.cardTextAffiliateFlashSaleSpecial.gone()
        binding.cardTextMall.gone()
        binding.cardTextArticle.gone()
        binding.cardTextPromotion.gone()
    }
    private fun setDimensionRatio(){
        binding.layoutImg.setDimensionRatio(dimensionRatio)
    }

    private fun setTypeEvent(){
        hideAllCardText()
        binding.cardTextEvent.visible()
        binding.cardTextEvent.requestLayout()
        model?.let { model ->
            binding.cardTextEvent.setModel(size?:CardEnumSize.SMALL, model)
        }
    }
    private fun setTypeAffiliate(){
        hideAllCardText()
        binding.cardTextAffiliate.visible()
        binding.cardTextAffiliate.requestLayout()
        model?.let { model ->
            binding.cardTextAffiliate.setModel(size?:CardEnumSize.SMALL, model)
        }
    }
    private fun setTypeAffiliateFlashSale(){
        hideAllCardText()
        binding.cardTextAffiliateFlashSale.visible()
        binding.cardTextAffiliateFlashSale.requestLayout()
        model?.let { model ->
            binding.cardTextAffiliateFlashSale.setModel(size?:CardEnumSize.SMALL, model)
        }
    }
    private fun setTypeAffiliateFlashSaleSpecial(){
        hideAllCardText()
        binding.cardTextAffiliateFlashSaleSpecial.visible()
        binding.cardTextAffiliateFlashSaleSpecial.requestLayout()
        model?.let { model ->
            binding.cardTextAffiliateFlashSaleSpecial.setModel(size?:CardEnumSize.SMALL, model)
        }
    }
    private fun setTypeAffiliateFlashSaleSoon(){
        hideAllCardText()
        binding.cardTextAffiliateFlashSaleSoon.visible()
        binding.cardTextAffiliateFlashSaleSoon.requestLayout()
        model?.let { model ->
            binding.cardTextAffiliateFlashSaleSoon.setModel(size?:CardEnumSize.SMALL, model)
            binding.cardTextAffiliateFlashSaleSoon.setOnClickListener {
                eventNotifyClickListener?.invoke(it)
            }
        }
    }
    private fun setTypeMall(){
        hideAllCardText()
        binding.cardTextMall.visible()
        binding.cardLike.gone()
        binding.cardTextMall.requestLayout()
        model?.let { model ->
            binding.cardTextMall.setModel(size?:CardEnumSize.SMALL, model)
        }
    }
    private fun setTypeArticle(){
        hideAllCardText()
        binding.cardTextArticle.visible()
        binding.cardTextArticle.requestLayout()
        model?.let { model ->
            binding.cardTextArticle.setModel(size?:CardEnumSize.SMALL, model)
        }
    }
    private fun setTypePromotion(){
        hideAllCardText()
        binding.cardTextPromotion.visible()
        binding.cardTextPromotion.requestLayout()
        model?.let { model ->
            binding.cardTextPromotion.setModel(size?:CardEnumSize.SMALL, model)
        }
    }

}
